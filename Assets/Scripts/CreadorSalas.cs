using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;
using static UnityEngine.Rendering.DebugUI.Table;
using Random = UnityEngine.Random;

public class CreadorSalas : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Sala;
    [SerializeField]
    private GameObject m_Sala_1;
    [SerializeField]
    private GameObject m_Sala_2;
    [SerializeField]
    private GameObject m_Sala_3;
    [SerializeField]
    private GameObject m_Sala_4;
    [SerializeField]
    private GameObject puerta;
    [SerializeField]
    private Tilemap tilemap;

    public enum TipoSala
    {
        BOSS, INICIAL, LOOT, ENEMIGOS, TIENDA
    }

    private int[,] matrix = new int[100, 100];
    private int numeroDeSala = 0;
    [SerializeField]
    private int maxSala = 20;
    // Start is called before the first frame update
    void Start()
    {
        RellenarMatriz();
        GenerarMapa(20,20,0);
        PrintMapa();
        GenSala();
    }

    private void PrintMapa()
    {
        string f = "";
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                f += matrix[i,j] + " ";
            }
            f += "\n";
        }
        Debug.Log(f);   
    }
    

    // Update is called once per frame


    private void RellenarMatriz() {
        for (int i = 0; i < matrix.GetLength(0); i++) {
            for (int j = 0; j < matrix.GetLength(1); j++) {
                matrix[i, j] = 0;
            }
        }
    }

    private void GenerarMapa(int x, int y, int sala) {
        if (maxSala != 0)
        {
            if (numeroDeSala == 0)
            {
                matrix[x, y] = 1;
                numeroDeSala++;
                maxSala--;
            }

            else if (numeroDeSala == 19)
            {
             
                matrix[x, y] = 5;
                numeroDeSala++;
                maxSala--;
            }
            else
            {
                matrix[x, y] = sala;
            }


            List<int> PuertasAlrededor;
            GetPuertasAlrededor(out PuertasAlrededor, x, y);
            CambiarMatriz(PuertasAlrededor, x, y);
        }
        else {
            Debug.Log("AAAAA");
            return; }
    }

    private void CambiarMatriz(List<int> puertasAlrededor, int x, int y)
    {
        int salaLado;
      
        foreach (int i in puertasAlrededor) {
            if (maxSala > 0) { 
                switch (i)
                {
                    case 1:
                        salaLado = Random.Range(2, 5);

                        if (salaLado == matrix[x, y])
                        {

                            ponerSalaEnUno(x, y - 1, salaLado);

                        }
                        else
                        {
                            if (matrix[x, y - 1] == 0) { 
                            matrix[x, y - 1] = 9;
                            ponerSalaEnUno(x, y - 2, salaLado);
                            numeroDeSala++;
                            maxSala--;
                        }
                        }
                        break;
                    case 2:
                        salaLado = Random.Range(2, 5);
                        if (salaLado == matrix[x, y])
                        {
                            ponerSalaEnUno(x+1, y, salaLado);
                         
                        }
                        else
                        {
                            if (matrix[x + 1, y] == 0) {
                                matrix[x + 1, y] = 9;
                                ponerSalaEnUno(x + 2, y, salaLado);
                                numeroDeSala++;
                                maxSala--;
                            }
                         
                        }
                        break;
                    case 3:
                        salaLado = Random.Range(2, 5);
                        if (salaLado == matrix[x, y])
                        {
                            ponerSalaEnUno(x, y + 1, salaLado);
                         
                        }
                        else
                        {
                            if (matrix [x, y + 1] == 0)
                            {
                                matrix[x, y + 1] = 9;

                                ponerSalaEnUno(x, y + 2, salaLado);
                                numeroDeSala++;
                                maxSala--;
                            }
                        }
                        break;
                    case 4:
                        salaLado = Random.Range(2, 5);
                        if (salaLado == matrix[x, y])
                        {
                            ponerSalaEnUno(x - 1, y, salaLado);

                        }
                        else
                        {
                            if (matrix[x - 1, y] == 0)
                            {
                                matrix[x - 1, y] = 9;
                                ponerSalaEnUno(x - 2, y, salaLado);
                                numeroDeSala++;
                                maxSala--;
                            }
                        }
                        break;

                }
            }
        }
    }
    private void ponerSalaEnUno(int row, int col, int salaLado)
    {
        Debug.Log(numeroDeSala);
        if (matrix[row, col] != 0)
            return;

        else
        {
            GenerarMapa(row, col, salaLado);
        }
    }


    private void GetPuertasAlrededor(out List<int> puertas, int x , int y)
    {
        int puertasMax = 5;
        if (matrix[x+2, y] != 0 || matrix[x + 1, y] != 0) { puertasMax--; }
        if (matrix[x-2, y] != 0 || matrix[x - 1, y] != 0) { puertasMax--; }
        if (matrix[x, y+2] != 0 || matrix[x, y + 1] != 0 ) { puertasMax--; }
        if (matrix[x, y-2] != 0 || matrix[x, y - 1] != 0) { puertasMax--; }
        puertas = new List<int>();
        int numeroPuertas = Random.Range(1, puertasMax);

        do
        {
            int puerta = Random.Range(1, 5);
            int encontrado = puertas.FirstOrDefault<int>(n => n == puerta);

            if (encontrado == 0)
            {
                puertas.Add(puerta);
                numeroPuertas--;
            }
          
        } while (numeroPuertas > 0);

        int c = 0;
        puertas.Shuffle();
        foreach (int puerta in puertas)
        {
            switch (puerta)
            {
                case 1:
                    if (matrix[x + 1, y] == 9) { c++; }
                    break;
                case 2:
                    if (matrix[x, y - 1] == 9) { c++; }
                    break;
                case 3:
                    if (matrix[x, y + 1] == 9) { c++; }
                    break;
                case 4:
                    if (matrix[x - 1, y] == 9) { c++; }
                    break;
            }
        }
        if (c > 0)
        {
            GetPuertasAlrededor(out puertas, x, y);
        }

    }

    public void GenSala()
    {
        for (int x = 0; x < matrix.GetLength(0); x++ ) {
            for (int y = 0; y < matrix.GetLength(1); y++)
            {
                InstanciarSala(x, y, matrix[x,y]);
                    
                
            }
        }
    }

    private void InstanciarSala(int x, int y, int tipoSala)
    {
        int posicionX = ((20 - x) * 1);
        int posicionY = ((20 - y) * 1);
        GameObject sala = null;
        switch (tipoSala)
        {
            case 1:
                sala = Instantiate(m_Sala_1);
                break;
            case 2:
                sala = Instantiate(m_Sala_2);
                break; 
            case 3:
                sala = Instantiate(m_Sala_3);
                break;
            case 4:
                sala = Instantiate(m_Sala_4);
                break;
            case 5:
                sala = Instantiate(m_Sala);
                break;
            case 9:
                sala = Instantiate(puerta);
                break;
            default:
                break;

        }
        if (sala != null)
        {
            sala.transform.position = new Vector3(posicionX, posicionY, 0);
        }
    }
}
