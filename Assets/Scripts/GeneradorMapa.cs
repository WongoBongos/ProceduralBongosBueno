using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

public class GeneradorMapa : MonoBehaviour
{
    [SerializeField]
    //offset from the perlin map
    private float m_OffsetX;
    [SerializeField]
    private float m_OffsetY;

    //size of the area we will paint
    [SerializeField]
    private int m_Width;
    [SerializeField]
    private int m_Height;

    //Scale
    [SerializeField]
    private float m_Scale = 1000f;
    //Scale
    [SerializeField]
    private float m_Frequency = 10f;

    //octaves
    private const int MAX_OCTAVES = 8;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_Octaves = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_Lacunarity = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_Persistence = 0.5f;
    [Tooltip("Do the octaves carve the terrain?")]
    [SerializeField]
    private bool m_Carve = true;
    [SerializeField]
    private float[] levels;
    [SerializeField]
    private Tile[] tiles;
    [SerializeField]
    private Tilemap tilemap;

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
            StartCoroutine("GenTerrain");
    }
    public IEnumerator GenTerrain()
    {
        for (int y = 0; y < m_Height; y++)
        {
            yield return null;
            for (int x = 0; x < m_Width; x++)
            {
                float xCoord = m_OffsetX + (x / m_Scale) * m_Frequency;
                float yCoord = m_OffsetY+ (y / m_Scale) * m_Frequency;
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                //Acte seguit calculem les octaves
                for (int octave = 1; octave <= m_Octaves; octave++)
                {
                    //La Lacunarity afecta a la freqencia de cada subseqent octava. El limitem a [2,3] de forma
                    //que cada nou valor sigui x2 o x3 de la freqencia anterior (doble o triple de soroll)
                    float newFreq = m_Frequency * m_Lacunarity * octave;
                    float xOctaveCoord = m_OffsetX + (x / m_Scale) * newFreq;
                    float yOctaveCoord = m_OffsetY + (y / m_Scale) * newFreq;

                    //valor base de l'octava
                    float octaveSample = Mathf.PerlinNoise(xOctaveCoord, yOctaveCoord);

                    //La Persistence afecta a l'amplitud de cada subseqent octava. El limitem a [0.1, 0.9] de forma
                    //que cada nou valor afecti menys al resultat final.
                    //addicionalment, farem que el soroll en comptes de ser un valor base [0,1] sigui [-0.5f,0.5f]
                    //i aix pugui sumar o restar al valor inicial
                    octaveSample = (octaveSample - .5f) * (m_Persistence / octave);

                    //acumulaci del soroll amb les octaves i base anteriors
                    sample += octaveSample;
                }
                tilemap.SetTile(new Vector3Int(x, y, 0), SelectTile(sample));
            }
        }
    }
    Tile SelectTile(float p)
    {
        for (int i = 0; i < levels.Length; i++)
        {
            if (p < levels[i]) return tiles[i];
        }
        return null;
    }

}
